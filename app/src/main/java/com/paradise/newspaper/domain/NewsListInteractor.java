package com.paradise.newspaper.domain;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.paradise.newspaper.data.entity.news.*;
import com.paradise.newspaper.data.network.StubFB;
import com.paradise.newspaper.ress.Storage;
import com.paradise.newspaper.ress.StorageListener;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import java.util.*;

public class NewsListInteractor implements StorageListener {
    private final StubFB stubFB;
    private MutableLiveData<List<News>> newsLiveData = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SharedPreferences sharedPreferences;

    private Flowable<List<Youtube>> youtube;
    private Flowable<List<Basic>> basic;
    private Flowable<List<Valute>> valute;
    private Flowable<Forecast> weather;

    public NewsListInteractor(StubFB stubFB, Context context) {
        Storage.registerStorageListener(this);
        sharedPreferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
        getValute();
        this.stubFB = stubFB;
    }

    private void getValute() {
        String saved = sharedPreferences.getString("pref", "empty");
        if (!saved.equals("empty")) {
            String[] res = saved.split(":");
            Valute obj = new Valute(res[0], res[1], res[2]);
            valute = Flowable.just(Arrays.asList(obj));
        }
    }

    public MutableLiveData<List<News>> getNewsLiveData() {
        pickNewsFromAllSource();
        setLiveData();
        return newsLiveData;
    }

    private void pickNewsFromAllSource() {
        weather = stubFB.getNewsWeatherDao().getAll().map(Forecast::new);
        youtube = stubFB.getNewsYoutubeDao().getAll();
        basic = stubFB.getNewsBasicDao().getAll();
    }

    //Слитые новости сеттим в LiveData
    private void setLiveData() {
        compositeDisposable.add(sortLiveData().subscribeOn(Schedulers.io())
                .subscribe(result -> newsLiveData.postValue(result),
                        error -> Log.e("tag_", error.getLocalizedMessage())));
    }

    private Flowable<List<News>> sortLiveData() {
        return combineNews(youtube, basic, valute, weather).map(newsList -> {
            Collections.sort(newsList,
                    (o1, o2) -> Long.compare(o1.getCreationDate(), o2.getCreationDate()));
            return newsList;
        });
    }

    //Cливаем полученные новости
    private Flowable<List<News>> combineNews(Flowable<List<Youtube>> youtube,
                                             Flowable<List<Basic>> basic,
                                             Flowable<List<Valute>> valute,
                                             Flowable<Forecast> weather) {

        return Flowable.combineLatest(youtube, basic, valute, weather, (youtubes, basics, valutes, forecast) -> {
            List<News> result = new ArrayList<>();
            result.addAll(youtubes);
            result.addAll(basics);
            result.addAll(valutes);
            result.add(forecast);
            return result;
        });
    }

    public void removeStorageListener() {
        Storage.unregisterStorageListener(this);
    }

    @Override
    public void onDataChanged() {
        getValute();
        pickNewsFromAllSource();
        setLiveData();
    }
}
