package com.paradise.newspaper.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.paradise.newspaper.data.entity.FirstLaunch;

import java.util.List;

@Dao
public interface FirstLaunchDao {

    @Query("SELECT * FROM firstlaunch")
    List<FirstLaunch> getAll();

    @Insert
    void insert(FirstLaunch firstLaunch);
}
