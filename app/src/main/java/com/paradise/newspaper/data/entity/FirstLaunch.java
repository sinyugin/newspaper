package com.paradise.newspaper.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Calendar;

@Entity
public class FirstLaunch {

    public FirstLaunch() {
        date = Calendar.getInstance().getTimeInMillis();
    }

    @PrimaryKey(autoGenerate = true)
    public long id;

    public long date;
}
