package com.paradise.newspaper.data.entity.news;

public interface News {
    int TYPE_BASIC_NEWS = 101;
    int TYPE_VALUTE_EXCHANGE = 102;
    int TYPE_YOUTUBE = 103;
    int TYPE_WEATHER = 104;

    int getType();
    long getCreationDate();
}
