package com.paradise.newspaper.data.network;

import com.paradise.newspaper.common.Constants;
import com.paradise.newspaper.data.dao.*;
import com.paradise.newspaper.data.entity.news.Basic;
import com.paradise.newspaper.data.entity.news.Part;
import com.paradise.newspaper.data.entity.news.Youtube;

import javax.inject.Inject;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.paradise.newspaper.common.Constants.*;

/**
 * Mock данные
 */

public class StubFB {

    private final FirstLaunchDao firstLaunchDao;
    private final NewsBasicDao newsBasicDao;
    private final NewsYoutubeDao newsYoutubeDao;
    private final NewsWeatherDao newsWeatherDao;
    private final PartDao partDao;

    @Inject
    public StubFB(FirstLaunchDao firstLaunchDao,
                  NewsBasicDao newsBasicDao,
                  NewsYoutubeDao newsYoutubeDao,
                  NewsWeatherDao newsWeatherDao,
                  PartDao partDao) {
        this.firstLaunchDao = firstLaunchDao;
        this.newsBasicDao = newsBasicDao;
        this.newsYoutubeDao = newsYoutubeDao;
        this.newsWeatherDao = newsWeatherDao;
        this.partDao = partDao;
    }

    public void insertNewsBasic() {
        new Thread(() -> newsBasicDao.insert(new Basic("В Пензе этнодиктант написали губернатор, атаман и студенты из Киргизии",
                "https://pravda-news.ru/upload/resize_cache/iblock/2cc/580_390_15f6c2ca7f349d878e77366566b5d05c8/2cc78ed32a05b60c70c6afaeb43c5ea4.jpg",
                new Date().getTime() / 1000L,
                "Новости"))).start();
    }

    public void insertNewsYoutube() {
        new Thread(() -> newsYoutubeDao.insert(new Youtube("XlGsB-hiQc8", new Date().getTime() / 1000L ))).start();
    }

    public void insertPartDao(){
        Part photo1 = new Part("1", 1, PART_PHOTO, "https://pravda-news.ru/upload/resize_cache/iblock/2cc/580_390_15f6c2ca7f349d878e77366566b5d05c8/2cc78ed32a05b60c70c6afaeb43c5ea4.jpg");
        Part title = new Part("1", 2, PART_TITLE, "В Пензе этнодиктант написали губернатор, атаман и студенты из Киргизии");
        Part text1 = new Part("1", 3, PART_TEXT, "Сегодня, 2 ноября, в Пензе написали «Большой этнографический диктант», приуроченный к празднованию Дня народного единства. Любители этнографии собрались на пяти площадках — в областной библиотеке им. М. Лермонтова, Доме молодежи, ПГУ, социально-педагогическом колледже, школе села Бессоновка. \n" +
                "\n" +
                "Каждый участник отвечал на 30 вопросов, 10 из которых касались Пензенской области. Задания были в виде тестов, на обдумывание давалось 45 минут. \n" +
                "\n" +
                "В библиотеке им. М. Лермонтова в написании диктанта приняли участие школьники, студенты и просто взрослые пензенцы. Кстати, испытать себя решили и студенты академии архитектуры и строительства из Киргизии. Например, Аджигит Акмолдоев выбрал такой способ, чтобы больше узнать о России, ее государственном и национальном устройстве.\n" +
                "\n" +
                "В написании этнодиктанта принял участие и губернатор Пензенской области Иван Белозерцев.\n" +
                "\n" +
                "— Вопросы интересные и довольно сложные, — отметил он. – Казалось бы, легко ответить, чем угощают новобрачных у татар и башкир. Был большой соблазн выбрать чак-чак, но я поставил другой ответ. Потом узнаю, правильно ли сделал. Из касающихся нашего края самый легкий вопрос «Какое самое крупное татарское село в области?» или «В каком районе проводятся кулачные бои на Троицу?». Это я знаю хорошо. \n" +
                "\n" +
                "Иван Александрович считает, что написание подобных диктантов полезно для людей любого возраста. Ведь знать историю своей страны, своего региона это важно и нужно. ");
        Part photo2 = new Part("1", 4, PART_PHOTO, "https://pravda-news.ru/upload/DSC_0231.jpg");
        Part text2 = new Part("1", 5, PART_TEXT, "Многие участники, как и губернатор, тоже отметили сложность диктанта. Затруднения вызывали такие вопросы, как численность коренных народов Севера или  кто из всемирно известных исследователей внес особый вклад в этнографическое изучение народов Восточной Сибири. \n" +
                "\n" +
                "Среди вопросов, касающихся Пензенской области, немногие ответили на вопрос, какое село полностью является чувашским.\n" +
                "\n" +
                "Атаман хутора Колышлейский Алексей Поляков, его заместитель Олег Киселев и ученик кадетской школы поселка «Родниковский» Олег Беляков приехали втроем проверить свои знания и сошлись на том, что надо глубже изучать историю страны и проживающих на ее территории народов. Стимулом к этому и явился этнодиктант.\n" +
                "\n" +
                "Результаты диктанта будут известны в начале декабря.");
        Part subtitle = new Part("1", 6, PART_SUBTITLE, "Как принять участие в Этнодиктанте?");
        Part listItem1 = new Part("1", 7, PART_LIST_ITEM, "Шаг 1");
        Part text3 = new Part("1", 8, PART_TEXT, "Набраться смелости и сказать себе: \" Я найду время пройти этот диктант \" ");
        Part listItem2 = new Part("1", 9, PART_LIST_ITEM, "Шаг 2");
        Part text4 = new Part("1", 10, PART_TEXT, "Подать заявку на участие в диктанте");
        Part listItem3 = new Part("1", 11, PART_LIST_ITEM, "Шаг 3");
        Part text5 = new Part("1", 12, PART_TEXT, "Самое сложное. НЕ ПРОСПАТЬ и прийти!");

        List<Part> list = Arrays.asList(photo1, title, text1, photo2, text2, subtitle, listItem1, text3, listItem2, text4, listItem3, text5);
        new Thread(() -> partDao.insert(list)).start();
    }

    public FirstLaunchDao getFirstLaunchDao() {
        return firstLaunchDao;
    }

    public NewsBasicDao getNewsBasicDao() {
        return newsBasicDao;
    }

    public NewsYoutubeDao getNewsYoutubeDao() {
        return newsYoutubeDao;
    }

    public NewsWeatherDao getNewsWeatherDao() {
        return newsWeatherDao;
    }

    public PartDao getPartDao() {
        return partDao;
    }
}

