package com.paradise.newspaper.data.entity.weather;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import com.paradise.newspaper.data.entity.news.News;

import java.util.Date;

@Entity
public class Weather implements News, Parcelable {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String city;
    private String description;
    private String icon;
    private int temp;
    private int maxTemp;
    private int minTemp;
    private int humidity;
    private int pressure;
    private int windSpeed;
    private long dt;               //погода на конкретное время по UNIX
    private String dtTxt;         //погода на конкретное время по UNIX в String
    private long creationDate;

    public Weather() {
        creationDate = new Date().getTime();
        city = "Каменка";
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(int maxTemp) {
        this.maxTemp = maxTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(int minTemp) {
        this.minTemp = minTemp;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    @Override
    public int getType() {
        return News.TYPE_WEATHER;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getCreationDate() {
        return creationDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.city);
        dest.writeString(this.description);
        dest.writeString(this.icon);
        dest.writeInt(this.temp);
        dest.writeInt(this.maxTemp);
        dest.writeInt(this.minTemp);
        dest.writeInt(this.humidity);
        dest.writeInt(this.pressure);
        dest.writeInt(this.windSpeed);
        dest.writeLong(this.dt);
        dest.writeString(this.dtTxt);
        dest.writeLong(this.creationDate);
    }

    protected Weather(Parcel in) {
        this.id = in.readLong();
        this.city = in.readString();
        this.description = in.readString();
        this.icon = in.readString();
        this.temp = in.readInt();
        this.maxTemp = in.readInt();
        this.minTemp = in.readInt();
        this.humidity = in.readInt();
        this.pressure = in.readInt();
        this.windSpeed = in.readInt();
        this.dt = in.readLong();
        this.dtTxt = in.readString();
        this.creationDate = in.readLong();
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel source) {
            return new Weather(source);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };
}
