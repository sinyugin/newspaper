package com.paradise.newspaper.data.entity.news;

import com.paradise.newspaper.data.entity.weather.Weather;

import java.util.List;

public class Forecast implements News {

    private boolean expanded;

    private List<Weather> weatherList;
    private long creationDate;

    public Forecast(List<Weather> weatherList) {
        this.weatherList = weatherList;
        creationDate = weatherList.get(0).getDt() / 1000L;
    }

    @Override
    public int getType() {
        return News.TYPE_WEATHER;
    }

    @Override
    public long getCreationDate() {
        return creationDate;
    }

    public Weather getDay0() {
        return weatherList.get(0);
    }

    public Weather getDay1() {
        return weatherList.get(8);
    }

    public Weather getDay2() {
        return weatherList.get(16);
    }

    public Weather getDay3() {
        return weatherList.get(24);
    }

    public Weather getDay4() {
        return weatherList.get(32);
    }

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
