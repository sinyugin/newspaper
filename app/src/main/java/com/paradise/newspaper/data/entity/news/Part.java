package com.paradise.newspaper.data.entity.news;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Part {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String uuid;
    private int order;
    private int type;
    private String payload;


    public Part(String uuid, int order, int type, String payload) {
        this.uuid = uuid;
        this.order = order;
        this.type = type;
        this.payload = payload;
    }

    public long getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(long id) {
        this.id = id;
    }
}
