package com.paradise.newspaper.data.entity.news;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class Basic implements News {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private long creationDate;
    private String title;
    private String url;
    private String category;

    public Basic(String title, String url, long creationDate, String category) {
        this.title = title;
        this.url = url;
        this.creationDate = creationDate;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int getType() {
        return News.TYPE_BASIC_NEWS;
    }

    public String getUrl() {
        return url;
    }

    public String getCategory() {
        return category;
    }
}
