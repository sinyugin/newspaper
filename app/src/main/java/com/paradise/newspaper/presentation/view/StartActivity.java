package com.paradise.newspaper.presentation.view;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.data.entity.FirstLaunch;
import com.paradise.newspaper.presentation.viewmodel.GlobalViewModelFactory;
import com.paradise.newspaper.presentation.viewmodel.StartViewModel;
import com.paradise.newspaper.presentation.viewstate.FirstLaunchViewState;
import com.paradise.newspaper.ress.ValuteRequest;

import javax.inject.Inject;

public class StartActivity extends AppIntro2 {

    @Inject
    GlobalViewModelFactory factory;
    StartViewModel viewModel;

    SliderPage slide1, slide2, slide3, slide4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getAppComponent(this).inject(this);

        ValuteRequest.makeRequest();

        viewModel = ViewModelProviders.of(this, factory).get(StartViewModel.class);
        viewModel.getFirstLaunchLiveData().observe(this, this::renderFirstLaunch);

        initSliderPage();
        setCommonBackground();

        setSliderPageData(slide1, getString(R.string.slide1_title), getString(R.string.slide1_description), R.drawable.ic_slide1);
        setSliderPageData(slide2, getString(R.string.slide2_title), getString(R.string.slide2_description), R.drawable.ic_slide2);
        setSliderPageData(slide3, getString(R.string.slide3_title), getString(R.string.slide3_description), R.drawable.ic_slide3);
        setSliderPageData(slide4, getString(R.string.slide4_title), getString(R.string.slide4_description), R.drawable.ic_slide4);
    }

    private void renderFirstLaunch(FirstLaunchViewState state) {
        if (state == null)
            return;

        if (!state.isLaunchListEmpty()) {
            goToMainActivity();
        } else {
            viewModel.startForecastWorker();
        }
    }

    private void goToMainActivity() {
        MainActivity.goToMainActivity(this);
        finish();
    }

    private void initSliderPage() {
        slide1 = new SliderPage();
        slide2 = new SliderPage();
        slide3 = new SliderPage();
        slide4 = new SliderPage();
    }

    private void setSliderPageData(SliderPage page, String title, String description, int slideImage) {
        page.setTitle(title);
        page.setDescription(description);
        page.setImageDrawable(slideImage);
        page.setBgColor(Color.TRANSPARENT);
        addSlide(AppIntroFragment.newInstance(page));
    }

    private void setCommonBackground() {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.ic_sample_bg);
        imageView.setBackgroundColor(Color.BLACK);
        imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setBackgroundView(imageView);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        viewModel.checkIsLaunched(new FirstLaunch());
        viewModel.insertMockData();
        goToMainActivity();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        goToMainActivity();
    }
}
