package com.paradise.newspaper.presentation.view.custom_view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.utils.SpannableUtil;
import com.paradise.newspaper.common.utils.TimeUtil;
import com.paradise.newspaper.data.entity.news.Forecast;
import com.paradise.newspaper.data.entity.weather.Weather;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CollapsedCard extends LinearLayout {

    private ImageView ivArrowCloseOpen;
    private TextView tvTitle;
    private ConstraintLayout layoutDay0, layoutDay1, layoutDay2, layoutDay3, layoutDay4;
    private TextView tvDay0Name, tvDay1Name, tvDay2Name, tvDay3Name, tvDay4Name;
    private TextView tvDay0Date, tvDay1Date, tvDay2Date, tvDay3Date, tvDay4Date;
    private TextView tvDay0Temp, tvDay1Temp, tvDay2Temp, tvDay3Temp, tvDay4Temp;
    private ImageView ivDay0, ivDay1, ivDay2, ivDay3, ivDay4;
    public TabLayout tabLayout;

    public CollapsedCard(Context context) {
        super(context);
        init();
    }

    public CollapsedCard(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CollapsedCard(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public ImageView getIvArrowCloseOpen() {
        return ivArrowCloseOpen;
    }

    public void open() {
        View rootView = getChildAt(1);
        if (rootView == null) return;
        TranslateAnimation animation = new TranslateAnimation(0, 0, rootView.getHeight(), 0);
        animation.setDuration(100);
        animation.setFillAfter(true);
        rootView.startAnimation(animation);
        rootView.setVisibility(VISIBLE);
    }

    public void close() {
        View rootView = getChildAt(1);
        if (rootView == null) return;
        TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 0);
        animation.setDuration(100);
        animation.setFillAfter(true);
        rootView.startAnimation(animation);
        rootView.setVisibility(GONE);
    }

    private void init() {
        setOrientation(VERTICAL);
        setSaveEnabled(true);
        initView();
        setupTabClickListener();
    }

    private void initView() {
        View root = LayoutInflater.from(getContext()).inflate(R.layout.custom_forecast_view, this, true);
        ivArrowCloseOpen = root.findViewById(R.id.iv_show_arrow);
        tvTitle = root.findViewById(R.id.tv_title);

        layoutDay0 = root.findViewById(R.id.layout_day0);
        layoutDay1 = root.findViewById(R.id.layout_day1);
        layoutDay2 = root.findViewById(R.id.layout_day2);
        layoutDay3 = root.findViewById(R.id.layout_day3);
        layoutDay4 = root.findViewById(R.id.layout_day4);

        tvDay0Name = root.findViewById(R.id.tv_day0_name);
        tvDay1Name = root.findViewById(R.id.tv_day1_name);
        tvDay2Name = root.findViewById(R.id.tv_day2_name);
        tvDay3Name = root.findViewById(R.id.tv_day3_name);
        tvDay4Name = root.findViewById(R.id.tv_day4_name);

        tvDay0Date = root.findViewById(R.id.tv_day0_date);
        tvDay1Date = root.findViewById(R.id.tv_day1_date);
        tvDay2Date = root.findViewById(R.id.tv_day2_date);
        tvDay3Date = root.findViewById(R.id.tv_day3_date);
        tvDay4Date = root.findViewById(R.id.tv_day4_date);

        ivDay0 = root.findViewById(R.id.iv_day0);
        ivDay1 = root.findViewById(R.id.iv_day1);
        ivDay2 = root.findViewById(R.id.iv_day2);
        ivDay3 = root.findViewById(R.id.iv_day3);
        ivDay4 = root.findViewById(R.id.iv_day4);

        tvDay0Temp = root.findViewById(R.id.tv_day0_temp);
        tvDay1Temp = root.findViewById(R.id.tv_day1_temp);
        tvDay2Temp = root.findViewById(R.id.tv_day2_temp);
        tvDay3Temp = root.findViewById(R.id.tv_day3_temp);
        tvDay4Temp = root.findViewById(R.id.tv_day4_temp);

        tabLayout = root.findViewById(R.id.tabLayout);
    }

    private void setupTabClickListener() {
        layoutDay0.setOnClickListener(v -> setTabByDayClicking(layoutDay0));
        layoutDay1.setOnClickListener(v -> setTabByDayClicking(layoutDay1));
        layoutDay2.setOnClickListener(v -> setTabByDayClicking(layoutDay2));
        layoutDay3.setOnClickListener(v -> setTabByDayClicking(layoutDay3));
        layoutDay4.setOnClickListener(v -> setTabByDayClicking(layoutDay4));
    }

    private void setTabByDayClicking(View layout) {
        switch (layout.getId()) {
            case R.id.layout_day0:
                tabLayout.getTabAt(0).select();
                break;
            case R.id.layout_day1:
                tabLayout.getTabAt(1).select();
                break;
            case R.id.layout_day2:
                tabLayout.getTabAt(2).select();
                break;
            case R.id.layout_day3:
                tabLayout.getTabAt(3).select();
                break;
            case R.id.layout_day4:
                tabLayout.getTabAt(4).select();
                break;
        }
    }

    public void bindForecast(Forecast forecast) {
        bindTitle(tvTitle, forecast.getDay0(), forecast.getDay4());
        bindOneDayWeather(tvDay0Name, tvDay0Date, ivDay0, tvDay0Temp, forecast.getDay0());
        bindOneDayWeather(tvDay1Name, tvDay1Date, ivDay1, tvDay1Temp, forecast.getDay1());
        bindOneDayWeather(tvDay2Name, tvDay2Date, ivDay2, tvDay2Temp, forecast.getDay2());
        bindOneDayWeather(tvDay3Name, tvDay3Date, ivDay3, tvDay3Temp, forecast.getDay3());
        bindOneDayWeather(tvDay4Name, tvDay4Date, ivDay4, tvDay4Temp, forecast.getDay4());
    }

    private void bindTitle(TextView tvTitle, Weather day0, Weather day4) {
        tvTitle.setText(TimeUtil.forecastPeriod(day0, day4));
    }

    private void bindOneDayWeather(TextView name, TextView date, ImageView icon, TextView temp, Weather weather) {
        name.setText(TimeUtil.dayOfWeek(weather.getDt()));
        date.setText(TimeUtil.dayOfMonth(weather.getDt()));
        Glide.with(getContext())
                .load(String.format("%s%s%s", "http://openweathermap.org/img/w/", weather.getIcon(), ".png"))
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .transition(withCrossFade())
                .into(icon);
        temp.setText(SpannableUtil.parseMaxMinTemp(weather));
    }
}
