package com.paradise.newspaper.presentation.interfaces;

public interface OnViewPagerClickListener {
    void click();
}
