package com.paradise.newspaper.presentation.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.utils.SpannableUtil;
import com.paradise.newspaper.data.entity.weather.Weather;
import com.paradise.newspaper.presentation.interfaces.OnViewPagerClickListener;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class WeatherDayFragment extends Fragment {

    private static OnViewPagerClickListener listener;
    private static final String EXTRA_WEATHER = "weather";
    private ArrayList<Weather> listWeather;
    private LinearLayout linearLayoutistWeather;

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        listWeather = args.getParcelableArrayList(EXTRA_WEATHER);
    }

    public static WeatherDayFragment newInstance(ArrayList<Weather> listWeather, OnViewPagerClickListener onViewPagerClickListener) {
        WeatherDayFragment fragment = new WeatherDayFragment();
        listener = onViewPagerClickListener;
        Bundle args = new Bundle();
        args.putParcelableArrayList(EXTRA_WEATHER, listWeather);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.test_fragment_detail, container, false);
        linearLayoutistWeather = rootView.findViewById(R.id.linear_layout_list_weather);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        linearLayoutistWeather.removeAllViews();
        for (Weather weather : listWeather) {
            linearLayoutistWeather.addView(createSimpleWeatherRecord(weather));
        }
        linearLayoutistWeather.requestLayout();
    }

    private View createSimpleWeatherRecord(Weather weather) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.weather_simple_record, null);
        ImageView icon = view.findViewById(R.id.iv_icon);
        TextView temp = view.findViewById(R.id.tv_temp);
        Glide.with(getContext())
                .load(String.format("%s%s%s", "http://openweathermap.org/img/w/", weather.getIcon(), ".png"))
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .transition(withCrossFade())
                .into(icon);
        temp.setText(SpannableUtil.parseMaxMinTemp(weather));
        view.setOnClickListener(v -> listener.click());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
