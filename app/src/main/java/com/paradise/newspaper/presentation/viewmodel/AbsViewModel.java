package com.paradise.newspaper.presentation.viewmodel;

import android.arch.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;

public abstract class AbsViewModel extends ViewModel {

    //Добавил класс, чтоб не заморачиваться с методом onCleared.

    public final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCleared() {
        if (!compositeDisposable.isDisposed()) compositeDisposable.dispose();
        super.onCleared();
    }
}
