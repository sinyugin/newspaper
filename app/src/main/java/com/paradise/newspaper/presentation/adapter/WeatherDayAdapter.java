package com.paradise.newspaper.presentation.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.paradise.newspaper.data.entity.news.Forecast;
import com.paradise.newspaper.data.entity.weather.Weather;
import com.paradise.newspaper.presentation.interfaces.OnViewPagerClickListener;
import com.paradise.newspaper.presentation.view.fragments.WeatherDayFragment;

import java.util.ArrayList;
import java.util.List;

public class WeatherDayAdapter extends FragmentPagerAdapter {
    private final Forecast forecast;
    private final List<WeatherDayFragment> list = new ArrayList<>(5);

    public WeatherDayAdapter(FragmentManager fm, Forecast forecast, OnViewPagerClickListener listenerView) {
        super(fm);
        this.forecast = forecast;
        final Integer size = forecast.getWeatherList().size();
        list.add(WeatherDayFragment.newInstance(getWeatherRange(0, size - 32), listenerView));
        list.add(WeatherDayFragment.newInstance(getWeatherRange(size - 32, size - 24), listenerView));
        list.add(WeatherDayFragment.newInstance(getWeatherRange(size - 24, size - 16), listenerView));
        list.add(WeatherDayFragment.newInstance(getWeatherRange(size - 16, size - 8), listenerView));
        list.add(WeatherDayFragment.newInstance(getWeatherRange(size - 8, size), listenerView));
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    private ArrayList<Weather> getWeatherRange(int start, int end) {
        ArrayList<Weather> list = new ArrayList<>();
        for (int i = start; i < end; i++) {
            list.add(forecast.getWeatherList().get(i));
        }
        return list;
    }
}
