package com.paradise.newspaper.presentation.adapter;

import android.arch.lifecycle.Lifecycle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.paradise.newspaper.R;
import com.paradise.newspaper.data.entity.news.*;
import com.paradise.newspaper.data.entity.news.Forecast;
import com.paradise.newspaper.presentation.adapter.viewholder.*;
import com.paradise.newspaper.presentation.interfaces.OnViewPagerClickListener;
import com.paradise.newspaper.presentation.viewmodel.MainViewModel;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerFullScreenListener;

import java.util.ArrayList;
import java.util.List;

public class NewsListAdapter extends Adapter {
    private List<News> newsList;
    private FragmentManager fm;
    private RecyclerView recyclerView;
    private final ChangeBounds transition;
    private final Lifecycle lifecycle;
    private final MainViewModel mainViewModel;

    public NewsListAdapter(FragmentManager fm,
                           RecyclerView recyclerView,
                           Lifecycle lifecycle,
                           MainViewModel mainViewModel) {
        this.fm = fm;
        this.recyclerView = recyclerView;
        this.lifecycle = lifecycle;
        this.mainViewModel = mainViewModel;
        transition = new ChangeBounds();
        transition.setDuration(250);
    }

    public void setItems(List<? extends News> newsList) {
        if (this.newsList == null) this.newsList = new ArrayList<>();
        this.newsList.clear();
        this.newsList.addAll(newsList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return newsList.get(position).getType();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView;
        switch (viewType) {
            case News.TYPE_VALUTE_EXCHANGE:
                itemView = getItemView(viewGroup, R.layout.vh_news_valute_exchange);
                return new ValuteVH(itemView);
            case News.TYPE_BASIC_NEWS:
                itemView = getItemView(viewGroup, R.layout.vh_news_basic);
                return new BasicVH(itemView);
            case News.TYPE_YOUTUBE:
                itemView = getItemView(viewGroup, R.layout.vh_news_youtube);
                return new YouTubeVH(itemView);
            case News.TYPE_WEATHER:
                itemView = getItemView(viewGroup, R.layout.vh_news_forecast_5_days);
                return new WeatherVH(itemView, fm);
            default:
                itemView = getItemView(viewGroup, R.layout.vh_error);
                return new ErrorVH(itemView);
        }
    }

    private View getItemView(@NonNull ViewGroup viewGroup, int layout) {
        return LayoutInflater.from(viewGroup.getContext()).inflate(layout, viewGroup, false);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {

            case News.TYPE_VALUTE_EXCHANGE:
                Valute valute = (Valute) newsList.get(position);
                ((ValuteVH) viewHolder).bindView(valute);
                break;

            case News.TYPE_YOUTUBE:
                Youtube youtube = (Youtube) newsList.get(position);
                ((YouTubeVH) viewHolder).cueVideo(youtube.getVideoId());
                YouTubePlayerView playerView = (YouTubePlayerView) (((YouTubeVH) viewHolder).getYouTubePlayerView());
                playerView.getPlayerUIController().showFullscreenButton(true);
                playerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
                    @Override
                    public void onYouTubePlayerEnterFullScreen() {
                        mainViewModel.setBottomSheetState(false);
                    }
                    @Override
                    public void onYouTubePlayerExitFullScreen() {
                        mainViewModel.setBottomSheetState(true);
                    }
                });
                lifecycle.addObserver(playerView);
                break;

            case News.TYPE_BASIC_NEWS:
                Basic basic = (Basic) newsList.get(position);
                ((BasicVH) viewHolder).bindView(basic);
                ((BasicVH) viewHolder).itemView.setOnClickListener(v -> mainViewModel.showDetail(basic.getId()));
                break;

            case News.TYPE_WEATHER:
                Forecast forecast = (Forecast) newsList.get(position);
                WeatherVH vh = (WeatherVH) viewHolder;
                vh.bindView(forecast, () -> {
                    if (forecast.isExpanded());
                    collapseCard(forecast, vh);
                });
                vh.ivArrowCloseOpen.setOnClickListener(v -> {
                    if (!forecast.isExpanded())
                        expandCard(forecast, vh);
                    else
                        collapseCard(forecast, vh);
                });
                vh.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        if (!forecast.isExpanded())
                            expandCard(forecast, vh);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        if (!forecast.isExpanded())
                            expandCard(forecast, vh);
                        else
                            collapseCard(forecast, vh);
                    }
                });
                break;
            default:
                News news = newsList.get(position);
                ((ErrorVH) viewHolder).bindView(news);
        }
    }

    private void expandCard(Forecast forecast, WeatherVH vh) {
        vh.collapsedCard.open();
        vh.ivArrowCloseOpen.setImageResource(R.drawable.ic_arrow_up);
        forecast.setExpanded(true);
        TransitionManager.beginDelayedTransition(recyclerView, transition);
    }

    private void collapseCard(Forecast forecast, WeatherVH vh) {
        vh.collapsedCard.close();
        vh.ivArrowCloseOpen.setImageResource(R.drawable.ic_arrow_down);
        forecast.setExpanded(false);
        TransitionManager.beginDelayedTransition(recyclerView, transition);
    }

    @Override
    public int getItemCount() {
        return newsList == null ? 0 : newsList.size();
    }
}
