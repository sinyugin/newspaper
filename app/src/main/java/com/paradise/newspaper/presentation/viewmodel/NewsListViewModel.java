package com.paradise.newspaper.presentation.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import com.paradise.newspaper.data.entity.news.News;
import com.paradise.newspaper.domain.NewsListInteractor;

import javax.inject.Inject;
import java.util.List;

public class NewsListViewModel extends AbsViewModel {

    private final NewsListInteractor newsListInteractor;

    @Inject
    public NewsListViewModel(NewsListInteractor newsListInteractor) {
        this.newsListInteractor = newsListInteractor;
    }

    //Метод для получения данных во фрагменте
    public MutableLiveData<List<News>> getNewsLiveData() {
        return newsListInteractor.getNewsLiveData();
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        newsListInteractor.removeStorageListener();
    }
}
