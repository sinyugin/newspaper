package com.paradise.newspaper.presentation.adapter.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.paradise.newspaper.R;
import com.paradise.newspaper.data.entity.news.Basic;

public class BasicVH extends RecyclerView.ViewHolder {
    private ImageView photo;
    private TextView title, category, creationDate;
    private Context context;

    public BasicVH(@NonNull View itemView) {
        super(itemView);
        context = itemView.getContext();
        photo = itemView.findViewById(R.id.news_photo_item);
        title = itemView.findViewById(R.id.news_title_item);
        category = itemView.findViewById(R.id.news_category_item);
        creationDate = itemView.findViewById(R.id.news_creation_date_item);
    }

    public void bindView(Basic basic) {
        Glide.with(context).load(basic.getUrl()).thumbnail(0.1f).into(photo);
        title.setText(basic.getTitle());
        category.setText(basic.getCategory());
        creationDate.setText(String.valueOf(basic.getCreationDate()));
    }
}
