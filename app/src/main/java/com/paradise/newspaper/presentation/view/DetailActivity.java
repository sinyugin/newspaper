package com.paradise.newspaper.presentation.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import com.paradise.newspaper.R;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.common.Constants;
import com.paradise.newspaper.presentation.view.fragments.FragmentDetailNews;
import com.paradise.newspaper.presentation.view.fragments.FragmentDetailPhotoContainer;
import com.paradise.newspaper.presentation.viewmodel.DetailViewModel;
import com.paradise.newspaper.presentation.viewmodel.GlobalViewModelFactory;

import javax.inject.Inject;

public class DetailActivity extends AppCompatActivity {

    @Inject
    GlobalViewModelFactory factory;
    DetailViewModel viewModel;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        App.getAppComponent(this).inject(this);
        viewModel = ViewModelProviders.of(this, factory).get(DetailViewModel.class);

        viewModel.showFragmentLiveData().observe(this, name -> {
            if (name != null && name == Constants.DETAIL_NEWS) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, FragmentDetailNews.newInstance())
                        .commit();
            }
            if (name != null && name == Constants.DETAIL_PHOTO) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, FragmentDetailPhotoContainer.newInstance())
                        .commit();
            }
        });

        if (getIntent().getExtras() != null) {
            viewModel.setUUID(String.valueOf(getIntent().getLongExtra(Constants.EXTRA_NEWS_ID, 0)));
        }
    }

    public static void goToDetailActivity(AppCompatActivity activity, Long id) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(Constants.EXTRA_NEWS_ID, id);
        activity.startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof FragmentDetailPhotoContainer){
            viewModel.setCurrentShowingFragment(Constants.DETAIL_NEWS);
        }
        else {
            super.onBackPressed();
        }
    }
}
