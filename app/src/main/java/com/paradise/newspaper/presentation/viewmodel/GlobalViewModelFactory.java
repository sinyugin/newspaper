package com.paradise.newspaper.presentation.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

public class GlobalViewModelFactory implements ViewModelProvider.Factory {

    private final Map<Class<? extends ViewModel>, Provider<ViewModel>> viewModelProviders;

    @Inject
    public GlobalViewModelFactory(
            Map<Class<? extends ViewModel>, Provider<ViewModel>> viewModelProviders) {
        this.viewModelProviders = viewModelProviders;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Provider<? extends ViewModel> provider = viewModelProviders.get(modelClass);
        return (T) provider.get();
    }
}
