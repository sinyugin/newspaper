package com.paradise.newspaper.di.module;

import android.arch.persistence.room.Room;
import com.paradise.newspaper.common.App;
import com.paradise.newspaper.data.AppDatabase;
import com.paradise.newspaper.data.dao.*;
import com.paradise.newspaper.data.network.StubFB;
import com.paradise.newspaper.di.scope.AppScope;
import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private String db_name = "AppDatabase";

    @Provides
    @AppScope
    StubFB stubFB(FirstLaunchDao firstLaunchDao,
                  NewsBasicDao newsBasicDao,
                  NewsYoutubeDao newsYoutubeDao,
                  NewsWeatherDao newsWeatherDao,
                  PartDao partDao) {

        return new StubFB(firstLaunchDao, newsBasicDao, newsYoutubeDao, newsWeatherDao, partDao);
    }

    @Provides
    FirstLaunchDao firstLaunchDao(App app) {
        return Room.databaseBuilder(app, AppDatabase.class, db_name).build().firstLaunchDao();
    }

    @Provides
    NewsYoutubeDao newsYoutubeDao(App app) {
        return Room.databaseBuilder(app, AppDatabase.class, db_name).build().newsYoutubeDao();
    }

    @Provides
    NewsBasicDao newsBasicDao(App app) {
        return Room.databaseBuilder(app, AppDatabase.class, db_name).build().newsBasicDao();
    }

    @Provides
    NewsWeatherDao newsWeatherDao(App app) {
        return Room.databaseBuilder(app, AppDatabase.class, db_name).build().newsWeatherDao();
    }

    @Provides
    PartDao partDao(App app){
        return Room.databaseBuilder(app, AppDatabase.class, db_name).build().partDao();
    }
}
