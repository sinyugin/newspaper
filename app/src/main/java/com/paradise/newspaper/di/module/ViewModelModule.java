package com.paradise.newspaper.di.module;

import android.arch.lifecycle.ViewModel;
import com.paradise.newspaper.di.qualifier.ViewModelKey;
import com.paradise.newspaper.presentation.viewmodel.DetailViewModel;
import com.paradise.newspaper.presentation.viewmodel.MainViewModel;
import com.paradise.newspaper.presentation.viewmodel.NewsListViewModel;
import com.paradise.newspaper.presentation.viewmodel.StartViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(StartViewModel.class)
    abstract ViewModel bindStartViewModel(StartViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NewsListViewModel.class)
    abstract ViewModel bindNewsListViewModel(NewsListViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel.class)
    abstract ViewModel bindDetailViewModel(DetailViewModel detailViewModel);
}
