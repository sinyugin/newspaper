package com.paradise.newspaper.di.module;

import android.content.Context;
import com.paradise.newspaper.data.network.StubFB;
import com.paradise.newspaper.domain.NewsListInteractor;
import dagger.Module;
import dagger.Provides;

@Module
public class InteractorModule {
    @Provides
    NewsListInteractor combineAndSort(StubFB stubFB, Context context) {
        return new NewsListInteractor(stubFB, context);
    }
}
