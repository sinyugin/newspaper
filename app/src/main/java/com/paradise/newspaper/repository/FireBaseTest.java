package com.paradise.newspaper.repository;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FireBaseTest {

    static FireBaseTest instance;

    private FireBaseTest() {
    }

    public static FireBaseTest getInstance() {
        if (instance == null) instance = new FireBaseTest();
        return instance;
    }

    public void pushData() {
        long mTimeOfPressing = new Date().getTime();
        DatabaseReference mTestDB =
                FirebaseDatabase.getInstance().getReference("test").child("time_of_pressing");
        mTestDB.setValue(mTimeOfPressing);
    }

    public ArrayList<String> getData() {
        final ArrayList<String> dateList = new ArrayList<>();
        DatabaseReference child = FirebaseDatabase.getInstance().getReference("test");
        child.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    dateList.add(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(
                            new Date((long) childDataSnapshot.getValue())));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return dateList;
    }
}
