package com.paradise.newspaper.common;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import com.facebook.stetho.Stetho;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.paradise.newspaper.di.component.AppComponent;
import com.paradise.newspaper.di.component.DaggerAppComponent;
import com.paradise.newspaper.di.module.AppModule;
import com.paradise.newspaper.ress.ValuteRequest;
import com.paradise.newspaper.ress.Storage;

public class App extends MultiDexApplication {

    protected static App instance;

    private AppComponent component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Storage.getInstance(this);
        Stetho.initializeWithDefaults(this);
        AndroidThreeTen.init(this);
        ValuteRequest.registerRequestListener(Storage.getRequestListener());
    }

    public static App get() {
        return instance;
    }

    public static AppComponent getAppComponent(Context context) {
        return ((App) context.getApplicationContext()).component;
    }
}
