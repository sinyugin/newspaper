package com.paradise.newspaper.common.utils;

import com.paradise.newspaper.data.entity.weather.Weather;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {

    private static final SimpleDateFormat dayOfWeek = new SimpleDateFormat("EE");
    private static final SimpleDateFormat dayOfMonth = new SimpleDateFormat("dd.MM");

    public static long todayTimeStamp() {
        Calendar instance = Calendar.getInstance();
        roundToStartDay(instance);
        return instance.getTimeInMillis() / 1000L;
    }

    // Приводим дату погоды к 00:00 времения для сравнения.
    public static long weatherTimeStamp(long time) {
        Calendar instance = Calendar.getInstance();
        Date date = new Date();
        date.setTime(time * 1000L);
        instance.setTime(date);
        roundToStartDay(instance);
        return instance.getTimeInMillis() / 1000L;
    }

    public static void roundToStartDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static String dayOfWeek(long time) {
        Date date = new Date();
        date.setTime(time * 1000L);
        return dayOfWeek.format(date);
    }

    public static String dayOfMonth(long time) {
        Date date = new Date();
        date.setTime(time * 1000L);
        return dayOfMonth.format(date);
    }

    public static String forecastPeriod(Weather day0, Weather day4) {
        return String.format("%s %s %s %s", "Каменка", dayOfMonth(day0.getDt()), " - ", dayOfMonth(day4.getDt()));
    }
}
