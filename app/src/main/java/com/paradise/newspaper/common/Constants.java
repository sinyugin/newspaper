package com.paradise.newspaper.common;

public interface Constants {

    String CURRENT_POSITION = "current position of BottomNavigationView";
    String EXTRA_NEWS_ID = "id needed to get from DB appropriate news and show it in DetailActivity";


    // TYPE of PARTs
    int PART_PHOTO          = 100;
    int PART_TITLE          = 200;
    int PART_TEXT           = 300;
    int PART_SUBTITLE       = 400;
    int PART_LIST_ITEM      = 500;

    // SHOWING FRAGMENT
    int DETAIL_NEWS         = 100;
    int DETAIL_PHOTO        = 200;

}
