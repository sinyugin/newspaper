package com.paradise.newspaper.common.utils;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import com.paradise.newspaper.worker.ForecastWorker;

import java.util.concurrent.TimeUnit;

public class WorkUtil {

    public static OneTimeWorkRequest getOneTimeForecastWorkRequest() {
        return new OneTimeWorkRequest.Builder(ForecastWorker.class)
                .setConstraints(getNetworkConstraints())
                .build();
    }

    public static PeriodicWorkRequest getPeriodicForecastWorkRequest() {
        return new PeriodicWorkRequest.Builder(ForecastWorker.class, 30, TimeUnit.MINUTES)
                .setConstraints(getNetworkConstraints())
                .build();
    }

    private static Constraints getNetworkConstraints() {
        return new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
    }
}
